﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopRepository;

namespace ShopWebApp.Controllers
{
    public class InventoriesController : Controller
    {
        private shopEntities db = new shopEntities();
        

        // GET: Inventories
        public ActionResult Index()
        {
            var inventory = db.Inventory.Include(i => i.Product);
            ViewBag.Model = db.Inventory.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string code)
        {
            var inventory = db.Inventory.Include(i => i.Product);
            ViewBag.Model = db.Inventory.ToList();
            return View("Index", (object)code);
        }


        // GET: Inventories/Create
        public ActionResult Trade()
        {
            ViewBag.Product_Code = new SelectList(db.Products, "Code", "Code");
            return View();
        }

        // POST: Inventories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Trade(string Purchase, string Sell, [Bind(Include = "Product_Code,Size,Quantity")] Inventory inventory)
        {
            if (ModelState.IsValid)
            {
                //db.Inventory.Add(inventory);
                //db.SaveChanges();

                ShopRepo My = new ShopRepo();
                if (Purchase != null)
                {
                    My.Purchase(inventory.Product_Code, inventory.Size, inventory.Quantity);
                }
                if (Sell != null)
                {
                    My.Sell(inventory.Product_Code, inventory.Size, inventory.Quantity);
                }
                return RedirectToAction("Index");
            }

            ViewBag.Product_Code = new SelectList(db.Products, "Code", "Name", inventory.Product_Code);
            return View(inventory);
        }


        // GET: Inventories/Delete/5
        public ActionResult Delete(string code, string size)
        {
            if (code == null || size == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventory.Find(code,size);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string code, string size)
        {
            Inventory inventory = db.Inventory.Find(code,size);
            db.Inventory.Remove(inventory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public ActionResult Filter(string code)
        {
            var data = db.Inventory.ToList();
            if (!string.IsNullOrEmpty(code))
            {
                data = data.Where(i => i.Product_Code == code ).ToList();
            }
            return PartialView("_InvData", data);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
